import requests
import json

enctoken = ""
public_token = ""
__cfduid = "" 
kf_session = ""

#enter credentials 
userID = ""
password = ""
pin = ""

#call homePing() and login() before any function to fill in the global variables 

def homePing():
    url = "https://kite.zerodha.com/"
    payload = {}
    headers= {}
    response = requests.request("GET", url, headers=headers, data = payload)
    global __cfduid , kf_session
    __cfduid= response.cookies["__cfduid"]
    kf_session= response.cookies["kf_session"]

def login():
    url = "https://kite.zerodha.com/api/login"
    payload = 'user_id={}&password={}'.format(userID,password)
    headers = {
        'authority': 'kite.zerodha.com',
        'x-kite-version': '2.5.4',
        'accept': 'application/json, text/plain, */*',
        'x-kite-userid': userID,
        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36',
        'content-type': 'application/x-www-form-urlencoded',
        'origin': 'https://kite.zerodha.com',
        'sec-fetch-site': 'same-origin',
        'sec-fetch-mode': 'cors',
        'sec-fetch-dest': 'empty',
        'referer': 'https://kite.zerodha.com/',
        'accept-language': 'en-US,en;q=0.9',
        'cookie': '__cfduid={}; kf_session={}; user_id={}; __cfduid={}'.format(__cfduid,kf_session,userID,__cfduid)
    }
    response = requests.request("POST", url, headers=headers, data = payload)
    request_id = response.json()["data"]["request_id"]
    return twofa(request_id)

def twofa(request_id):
    url = "https://kite.zerodha.com/api/twofa"
    payload = 'user_id='+userID+'&request_id='+request_id+'&twofa_value='+pin
    headers = {
        'authority': 'kite.zerodha.com',
        'x-kite-version': '2.5.4',
        'accept': 'application/json, text/plain, */*',
        'x-kite-userid': userID,
        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36',
        'content-type': 'application/x-www-form-urlencoded',
        'origin': 'https://kite.zerodha.com',
        'sec-fetch-site': 'same-origin',
        'sec-fetch-mode': 'cors',
        'sec-fetch-dest': 'empty',
        'referer': 'https://kite.zerodha.com/',
        'accept-language': 'en-US,en;q=0.9',
        'cookie': '__cfduid={}; kf_session={}; user_id={}; __cfduid={}'.format(__cfduid,kf_session,userID,__cfduid)
    }
    response = requests.request("POST", url, headers=headers, data = payload)
    global enctoken
    global public_token
    public_token = response.cookies["public_token"]
    enctoken = response.cookies["enctoken"]
    return enctoken

def placeOrder(exchange,symbol,txType,orderType,quantity,price,product,validity,disclosedQuantity,triggerPrice,squareOff,stoploss,trailingStoploss,variety):
    url = "https://kite.zerodha.com/oms/orders/"+variety
    payload = 'exchange={}&tradingsymbol={}&transaction_type={}&order_type={}&quantity={}&price={}&product={}&validity={}&disclosed_quantity={}&trigger_price={}&squareoff={}&stoploss={}&trailing_stoploss={}&variety={}&user_id={}'.format(exchange,symbol,txType,orderType,quantity,price,product,validity,disclosedQuantity,triggerPrice,squareOff,stoploss,trailingStoploss,variety,userID)
    headers = {
        'authority': 'kite.zerodha.com',
        'x-kite-version': '2.5.4',
        'accept': 'application/json, text/plain, */*',
        'x-kite-userid': userID,
        'authorization': 'enctoken '+enctoken,
        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36',
        'content-type': 'application/x-www-form-urlencoded',
        'origin': 'https://kite.zerodha.com',
        'sec-fetch-site': 'same-origin',
        'sec-fetch-mode': 'cors',
        'sec-fetch-dest': 'empty',
        'referer': 'https://kite.zerodha.com/positions',
        'accept-language': 'en-US,en;q=0.9',
        'cookie': '__cfduid={}; kf_session={}; public_token={}; user_id={}; enctoken={};'.format(__cfduid,kf_session,public_token,userID,enctoken)
    }
    response = requests.request("POST", url, headers=headers, data = payload)
    if response.json()["status"] == "success":
        return response.json()
    else:
        return False

def margin():
    url = "https://kite.zerodha.com/oms/user/margins"
    payload = {}
    headers = {
        'authority': 'kite.zerodha.com',
        'x-kite-version': '2.5.4',
        'accept': 'application/json, text/plain, */*',
        'authorization': 'enctoken '+ enctoken,
        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36',
        'sec-fetch-site': 'same-origin',
        'sec-fetch-mode': 'cors',
        'sec-fetch-dest': 'empty',
        'referer': 'https://kite.zerodha.com/funds',
        'accept-language': 'en-US,en;q=0.9',
        'cookie': '__cfduid={}; kf_session={}; public_token={}; user_id={}; enctoken={};'.format(__cfduid,kf_session,public_token,userID,enctoken),
        'if-none-match': 'W/"mfxSHNzbMDZ1kjjV"'
    }
    response = requests.request("GET", url, headers=headers, data = payload)
    if response.json()["status"] == "success":
        return response.json()
    else:
        return False

def orderbook():
    url = "https://kite.zerodha.com/oms/orders"
    payload = {}
    headers = {
        'authority': 'kite.zerodha.com',
        'x-kite-version': '2.5.4',
        'accept': 'application/json, text/plain, */*',
        'authorization': 'enctoken '+enctoken,
        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36',
        'sec-fetch-site': 'same-origin',
        'sec-fetch-mode': 'cors',
        'sec-fetch-dest': 'empty',
        'referer': 'https://kite.zerodha.com/orders',
        'accept-language': 'en-US,en;q=0.9',
        'cookie': '__cfduid={}; kf_session={}; public_token={}; user_id={}; enctoken={};'.format(__cfduid,kf_session,public_token,userID,enctoken),
        'if-none-match': 'W/"9Vly5a35Xpon6YzL"'
    }
    response = requests.request("GET", url, headers=headers, data = payload)
    if response.json()["status"] == "success":
        return response.json()
    else:
        return False


def positions():
    url = "https://kite.zerodha.com/oms/portfolio/positions"
    payload = {}
    headers = {
        'authority': 'kite.zerodha.com',
        'x-kite-version': '2.5.4',
        'accept': 'application/json, text/plain, */*',
        'authorization': 'enctoken '+enctoken,
        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36',
        'sec-fetch-site': 'same-origin',
        'sec-fetch-mode': 'cors',
        'sec-fetch-dest': 'empty',
        'referer': 'https://kite.zerodha.com/positions',
        'accept-language': 'en-US,en;q=0.9',
        'cookie': '__cfduid={}; kf_session={}; public_token={}; user_id={}; enctoken={};'.format(__cfduid,kf_session,public_token,userID,enctoken),
        'if-none-match': 'W/"BYBqMLYe9NF8vpP9"'
    }
    response = requests.request("GET", url, headers=headers, data = payload)
    if response.json()["status"] == "success":
        return response.json()
    else:
        return False
